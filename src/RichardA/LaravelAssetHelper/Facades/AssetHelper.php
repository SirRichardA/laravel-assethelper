<?php

namespace RichardA\LaravelAssetHelper\Facades;

class AssetHelper extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'assethelper';
    }
}
