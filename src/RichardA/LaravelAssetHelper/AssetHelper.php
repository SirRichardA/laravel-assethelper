<?php

/**
 * A small class used for easy inclusion of stylesheets, images and scripts
 */

namespace RichardA\LaravelAssetHelper;

use Config;
use RichardA\LaravelAssetHelper\Exceptions\InvalidConfigItemException;

class AssetHelper
{
    /**
     * Get a config item for directories by the key name
     *
     * @param string $item
     * @return string
     *
     * @throws \RichardA\LaravelAssetHelper\Exceptions\InvalidConfigItemException
     */
    private static function getDirItem($item)
    {
        $configItem = Config::get("laravel-assethelper::dirs.$item");

        if(is_null($configItem))
        {
            throw new InvalidConfigItemException("Config item '$item' in dirs section does not exist");
        }

        return $configItem;
    }

    /**
     * Generate a path for the assets
     *
     * @param string $dir
     * @param string $file
     * @param string|null $ext
     * @return string
     */
    private static function createPath($dir, $file, $ext = null)
    {
        try
        {
            $dirItem = self::getDirItem($dir);
        }

        catch(InvalidConfigItemException $e)
        {
            return $e->getMessage();
        }

        return is_null($ext) ? "/$dir/$file" : "/$dir/$file.$ext";
    }

    /**
     * Get a stylesheet by its file name
     *
     * @param string $fileName
     * @return string
     */
    public static function css($fileName)
    {
        $path = self::createPath('css', $fileName, 'css');

        if(!file_exists(public_path($path)))
        {
            return sprintf('CSS File at path <strong>%s</strong> was not found', $path);
        }

        return sprintf('<link href="%s" rel="stylesheet" type="text/css" />', $path);
    }

    /**
     * Get a script by its file name
     *
     * @param string $fileName
     * @return string
     */
    public static function js($fileName)
    {
        $path = self::createPath('js', $fileName, 'js');

        if(!file_exists(public_path($path)))
        {
            return sprintf('JS File at path <strong>%s</strong> was not found', $path);
        }

        return sprintf('<script src="%s" type="text/javascript"></script>', $path);
    }

    /**
     * Get an image by its file name
     *
     * @param string $fileName
     * @param string $altText
     * @return string
     */
    public static function img($fileName, $altText = '')
    {
        $path = self::createPath('img', $fileName);

        if(!file_exists(public_path($path)))
        {
            return sprintf('Image at path <strong>%s</strong> was not found', $path);
        }

        return sprintf('<img src="%s" alt="%s" />', $path, $altText);
    }

    /**
     * Get a javascript plugin by its file name
     *
     * @param string $fileName
     * @return string
     */
    public function plugin($fileName)
    {
        $pluginsDir = self::getDirItem('plugins');

        return self::js("$pluginsDir/$fileName");
    }
}
