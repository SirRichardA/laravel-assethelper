<?php

namespace RichardA\LaravelAssetHelper;

use Illuminate\Support\ServiceProvider;

class LaravelAssetHelperServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('richard-a/laravel-assethelper');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['assethelper'] = $this->app->share(function($app)
		{
			return new AssetHelper;
		});

		$loader = \Illuminate\Foundation\AliasLoader::getInstance();
		$loader->alias('AssetHelper', 'RichardA\LaravelAssetHelper\Facades\AssetHelper');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
