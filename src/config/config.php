<?php

return array(
    "dirs" => array(
        "css" => "css",
        "js" => "js",
        "img" => "img",
        "plugins" => "plugins"
    )
);
