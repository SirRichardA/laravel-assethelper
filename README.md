# Laravel AssetHelper

A small class that's used for shorthand inclusion of stylesheets, javascript and images.

To install this package, install it through composer.

    "require": {
		"laravel/framework": "4.2.*",
		....
		"richard-a/laravel-assethelper": "1.*"
    },
    
Then add the service provider in your /app/config.app.php file.

    'providers' => array(
        ....

        'RichardA\LaravelAssetHelper\LaravelAssetHelperServiceProvider',
    )
    
## How to use

To use the AssetHelper class supports inclusion of stylesheets, javascript, plugins (usually jQuery) and images.

The following methods are available:

    AssetHelper::css($fileName);
    AssetHelper::js($fileName);
    AssetHelper::img($fileName, $altText);
    AssetHelper::plugin($fileName);
    
A note on `AssetHelper::plugin`, this method relies on `AssetHelper::js`, the plugin folder should recide within the directory the `js` method points to.

If at any point the AssetHelper methods can't find the file, it will output a message on the screen saying so to inform you.

By default, the method look at the following paths:

css: /public/css  
js: /public/js  
img: /public/img  
plugin: /public/js/plugin

The target paths are all configurable, just run the following command:

> php artisan config:publish richard-a/laravel-assethelper

You can then find the config in the **/app/config/packages/richard-a/laravel-assethelper** directory

Note that the plugin method also looks for the plugin directory inside of the js directory.